@extends('layouts.app')

@section('title', 'MNSL')

@section('body')
<div class="d-flex" id="wrapper">
        @auth
        <x-sidebar/>
        @endauth
        <div id="page-content-wrapper">
                <x-navbar/>
        
                <div class="container-fluid">
                        @yield('content')
                </div>
        </div>
</div>
@endsection

@section('scripts')
<script>
        $("#menu-toggle").click(function(e) {
        e.preventDefault();
        $("#wrapper").toggleClass("toggled");
        });
</script>

@endsection