@extends('layouts.main')

@section('content')

<div class="container-fluid my-4">
    <div class="my-2">
        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#deliveryModal">
            Create Delivery
        </button>
    </div>
    <table id="deliveryDatatable" class="table table-striped table-bordered dt-responsive nowrap" style="width:100%">
        <thead>
            <tr>
                <th>Delivery Date</th>
                <th>Meterial Slip</th>
                <th>Farm</th>
                <th>Type</th>
                <th>Quantity</th>
                <th>Remarks</th>
            </tr>
        </thead>
    </table>
    @include('components.delivery.create_modal')
    @include('components.delivery.view_modal')
</div>

@endsection
@section('scripts')
@parent
<script>
    $(document).ready(function() {
    var counter = 0;
    $('#deliveryDatatable').DataTable({
        'processing': true,
        'serverSide': true,
        'responsive': true,
        'stateSave': true,
        'ajax': '{{route("delivery.index")}}',
        'columns': [
            { "data": "delivery_date" },
            { 
                "data": null,
                "orderable": false,
                "render": function(data, type, row, meta) {
                    return '<a onclick="getDelivery('+data.id+')" data-toggle="modal" href="#viewDeliveryModal">'+data.material_slip+'</a>';
                }
            },
            { "data": "farm_name" },
            { "data": "delivery_type" },
            { "data": "actual_qty" },
            { "data": "remarks" },
          ],
        
   });
   $('.radio-type').on('click', function() {
      
   })
   $('select.type').on('click', function() {
      let selectedtype = $(this).children("option:selected").val();
      switch(parseInt(selectedtype)) {
        case 1:
          createNormal(1);
          break;
        case 2:
          createNormal(2);
          break;
        default:
      }
   })
    $('.btn-store').on('click', function(e) {
      e.preventDefault();
      let data = $('.form_delivery').serialize();
      storeDelivery(data);
    })
    $('#datetimepicker4').datetimepicker({
        format: 'YYYY-MM-DD',
    });
    $('.add_farm').on('click', function(e) {
        e.preventDefault();
        counter++
        $('#farm-field').append(
        '<h5>Divert to</h5>'+
        '<div id="row-'+counter+'" class="row">'+
            '<div class="col-sm-4">'+
              '<div class="form-group">'+
                '<input type="text" class="form-control" name="delivery_materialSlip[]" placeholder="Enter Material Slip">'+
              '</div>'+
            '</div>'+
            '<div class="col-sm-4">'+
              '<div class="form-group">'+
                '<select class="form-control" name="delivery_farm[]" id="delivery_farm">'+
                  '@foreach ($farms as $farm)'+
                  '<option value="{{$farm->id}}">{{$farm->farm_name}}</option>'+
                  '@endforeach'+
                '</select>'+
              '</div>'+
            '</div>'+
            '<div class="col-sm-4">'+
              '<div class="form-group">'+
                '<input type="text" class="form-control" name="delivery_quantity[]" placeholder="Enter Quantity">'+
              '</div>'+
            '</div>'+
        '</div>');
    })
});
function getDelivery(delivery_id) {
  $.ajax({
        url: '/delivery/'+ delivery_id,
        type: 'get',
        dataType: 'json',
        success: function(data){
  
            $('#delivery_code').text(data[0].code);

            $('#delivery_date').text(data[0].delivery_date);
            console.log(data[0]);
        }
    })
}
function createDivert() {
  $('.farm-field').empty();
}
function createNormal(number) {
  $('.farm-field').empty();

  for (var i = 0; i < number; i++) {
   

  $('.farm-field').append(
      '<div class="d-flex">'+
        '<h5 class="mr-auto">Delivery Details</h5>'+
      '</div>'+
      '<div class="form-group">'+
        '<label for="exampleInputEmail1">Material Slip</label>'+
        '<input type="text" autocomplete="off" class="form-control" name="delivery_materialSlip[]"placeholder="Enter Material Slip">'+
      '</div>'+
      '<div class="form-group">'+
        '<label for="farm_trip">Farm</label>'+
        '<select class="form-control" name="delivery_farm[]" id="delivery_farm">'+
          '@foreach ($farms as $farm)'+
          '<option value="{{$farm->id}}">{{$farm->farm_name}}</option>'+
          '@endforeach'+
        '</select>'+
      '</div>'+
      '<div class="form-group">'+
        '<label for="exampleInputEmail1">Quantity</label>'+
        '<input type="text" autocomplete="off" class="form-control" name="delivery_quantity[]" placeholder="Enter Quantity">'+
      '</div>'+
      '<div class="col-lg-1"></div>');
  }
}
function storeDelivery(data) {
  $.ajax({
      headers: { 'X-CSRF-TOKEN': $('input[name="_token"]').attr('content') },
      type:'post',
      url: '/delivery/store',
      data: data,
      dataType: "json",
      success: function(data) { 
        $("#deliveryModal").modal('hide');
        $('#deliveryDatatable').DataTable().ajax.reload();
        swal({
            text: "You clicked the button!",
            icon: "success",
            button: false,
            timer: 1000,
        });
        
      },
      error: function(xhr) {
          if(xhr.status === 422) {
              let errors = xhr.responseJSON.errors;
              $('.message').remove();
              $.each(errors, function (key, value) {
                  const errorSelector = $("#"+key);
                  errorSelector.after("<small id='message_"+key+"' class='message text-danger form-text text-muted'>"+value+"</small>");
              });
          }
      }
  })
}
</script>
@endsection