<div class="modal fade" id="deliveryModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Create Delivery</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <form class="form_delivery">
          {{ csrf_field() }}
          <div class="form-group">
            <label for="farm_trip">Date</label>
            <div class="input-group date" id="datetimepicker4" data-target-input="nearest">
                <input type="text" class="form-control datetimepicker-input" name="delivery_date" data-target="#datetimepicker4"/>
                <div class="input-group-append" data-target="#datetimepicker4" data-toggle="datetimepicker">
                    <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                </div>
            </div>
          </div>
          <div class="row">
            @foreach ($deliveryTypes as $deliveryType)
            <div class="col-md-4 d-flex justify-content-center">
              <div class="form-check">   
                <label class="form-check-label" for="type-{{$deliveryType->id}}">
                <input class="radio-type form-check-input" type="radio" name="exampleRadios" id="type-{{$deliveryType->id}}" data-id="{{$deliveryType->id}}" value="{{$deliveryType->id}}">
                  {{$deliveryType->name}}
                </label>
              </div>
            </div>
            @endforeach
          </div>
         
          
        
          {{-- <div class="form-group">
            <label for="farm_trip">Delivery Type</label>
            <select class="type form-control" name="delivery_type">
              @foreach ($deliveryTypes as $deliveryType)
              <option value="{{$deliveryType->id}}">{{$deliveryType->name}}</option>
              @endforeach
            </select>
          </div> --}}
          <div class="d-flex py-4">
            <h5 class="mr-auto">Delivery Details</h5>
          </div>
          <div class="form-group">
            <label for="exampleInputEmail1">Material Slip</label>
            <input type="text" autocomplete="off" class="form-control" name="delivery_materialSlip[]"placeholder="Enter Material Slip">
          </div>
          <div class="form-group">
            <label for="farm_trip">Farm</label>
            <select class="form-control" name="delivery_farm[]" id="delivery_farm">
              @foreach ($farms as $farm)
              <option value="{{$farm->id}}">{{$farm->farm_name}}</option>
              @endforeach
            </select>
          </div>
          <div class="form-group">
            <label for="exampleInputEmail1">Quantity</label>
            <input type="text" autocomplete="off" class="form-control" name="delivery_quantity[]" placeholder="Enter Quantity">
          </div>
          <div class="col-lg-1"></div>
            
          {{-- <div class="farm-field"></div> --}}
        </div>
        </form>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          <button type="submit" class="btn-store btn btn-success">Save changes</button>
        </div>
      </div>
    </div>
</div>