<div class="bg-primary border-right border-primary" id="sidebar-wrapper">
    <div class="sidebar-heading">MNSL Farm</div>
    <div class="list-group list-group-flush">
      <a href="#" class="list-group-item list-group-item-action bg-primary text-white">Dashboard</a>
      <a href="{{ route('farm.index') }}" class="list-group-item list-group-item-action bg-primary text-white">Farms</a>
      <a href="#" class="list-group-item list-group-item-action bg-primary text-white">Billing</a>
      <a href="{{ route('delivery.index')}}" class="list-group-item list-group-item-action bg-primary text-white">Delivery</a>
      <a href="#" class="list-group-item list-group-item-action bg-primary text-white">Reports</a>
      <a href="#" class="list-group-item list-group-item-action bg-primary text-white">Status</a>
    </div>
</div>