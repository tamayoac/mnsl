<div class="modal fade" id="farmModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Create Farm</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <form action="{{route('farmstore')}}" method="POST">
                {{ csrf_field() }}
                <div class="form-group">
                  <label for="farm_name">Name</label>
                  <input type="text" class="form-control" name="farm_name"  id="farm_name" placeholder="Enter Name">
                </div>
                <div class="form-group">
                  <label for="farm_location">Location</label>
                  <input type="text" class="form-control" name="farm_location" id="farm_location" placeholder="Enter Location">
                </div>
                <div class="form-group">
                    <label for="farm_distance">Distance</label>
                    <input type="text" class="form-control" name="farm_distance" id="farm_distance"placeholder="Enter Distance">
                  </div>
                  <div class="form-group">
                    <label for="farm_bags">Bags</label>
                    <input type="text" class="form-control" name="farm_bags" id="farm_bags" placeholder="Enter Bags">
                  </div>
                  <div class="form-group">
                    <label for="farm_rate">Rate</label>
                    <input type="text" class="form-control" name="farm_rate" id="farm_rate" placeholder="Enter Rate">
                  </div>
                  <div class="form-group">
                    <label for="farm_labor">Labor</label>
                    <input type="text" class="form-control" name="farm_labor" id="farm_labor" placeholder="Enter Labor">
                  </div>
                  <div class="form-group">
                    <label for="farm_trip">Trip</label>
                    <select class="form-control" name="farm_trip" id="farm_trip">
                      <option value="regular">Regular</option>
                      <option value="special">Special</option>
                    </select>
                  </div>
             
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-success">Save changes</button>
        </div>
        </form>
      </div>
    </div>
</div>