<div class="modal fade" id="updateFarmModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Update Farm</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
        <form class="form_update_farm">
            {{ csrf_field() }}
            {{ method_field('PUT')}}
            <div class="form-group">
              <label for="farm_name">Name</label>
              <input type="hidden" id="update_farm_id">
              <input type="text" class="form-control" name="update_farm_name"  id="update_farm_name" placeholder="Enter Name">
            </div>
            <div class="form-group">
              <label for="farm_location">Location</label>
              <input type="text" class="form-control" name="update_farm_location" id="update_farm_location" placeholder="Enter Location">
            </div>
            <div class="form-group">
              <label for="farm_distance">Distance</label>
              <input type="text" class="form-control" name="update_farm_distance" id="update_farm_distance"placeholder="Enter Distance">
            </div>
            <div class="form-group">
              <label for="farm_bags">Bags</label>
              <input type="text" class="form-control" name="update_farm_bags" id="update_farm_bags" placeholder="Enter Bags">
            </div>
            <div class="form-group">
              <label for="farm_rate">Rate</label>
              <input type="text" class="form-control" name="update_farm_rate" id="update_farm_rate" placeholder="Enter Rate">
            </div>
            <div class="form-group">
              <label for="farm_labor">Labor</label>
              <input type="text" class="form-control" name="update_farm_labor" id="update_farm_labor" placeholder="Enter Labor">
            </div>
            <div class="form-group">
              <label for="farm_trip">Trip</label>
              <select class="form-control" name="update_farm_trip" id="update_farm_trip">
                <option value="regular">Regular</option>
                <option value="special">Special</option>
              </select>
          </div>
        </div>
        </form>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          <button type="button" onclick="updateFarm()"class="btn btn-primary">Update changes</button>
        </div>
      </div>
    </div>
</div>