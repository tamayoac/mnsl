@extends('layouts.main')

@section('content')
<div class="container-fluid my-4">
        <div class="my-2">
            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#farmModal">
                Add Farm
            </button>
        </div>
      
      <table id="farmDatatable" class="table table-striped table-bordered dt-responsive nowrap" style="width:100%">
        <thead>
            <tr>
                <th>Name</th>
                <th>Location</th>
                <th>Distance</th>
                <th>Rate</th>
                <th>Bags</th>
                <th>Type</th>
                <th>Labor</th>
                <th>Status</th>
                <th width="200px">Action</th>
            </tr>
        </thead>
    </table>
    @include('components.farm.update_modal')
    @include('components.farm.create_modal')
</div>
@endsection

@section('scripts')
@parent
<script>

$(document).ready(function() {
   $('#farmDatatable').DataTable({
        'processing': true,
        'serverSide': true,
        'responsive': true,
        'stateSave': true,
        'ajax': '{{route("farm.index")}}',
        'columns': [
            { "data": "farm_name" },
            { "data": "farm_location" },
            { "data": "farm_distance" },
            { "data": "farm_rate" },
            { "data": "farm_bags" },
            { "data": "farm_type" },
            { "data": "farm_labor" },
            { "data": "status" },
            { 
                "data": null,
                "orderable": false,
                "render": function(data, type, row, meta) {
                    return "<button class='btn btn-primary mr-2' onclick=getFarm("+data.id+")><i class='fa fa-pencil'></i></button>"+
                           "<button class='btn btn-danger' onclick=removeFarm("+data.id+")><i class='fa fa-trash'></i></button>";
                }
            }
        ],
        
   });
});
function updateFarm() {
    $.ajax({
            headers: { 'X-CSRF-TOKEN': $('input[name="_token"]').attr('content') },
                type:'post',
                url: '/farm/' + $('#update_farm_id').val(),
                data: $('.form_update_farm').serialize(),
                dataType: "json",
                success: function(data) { 
                    $('#updateFarmModal').modal('hide');
                    $('#farmDatatable').DataTable().ajax.reload();
                    swal({
                        text: "You clicked the button!",
                        icon: "success",
                        button: false,
                        timer: 1000,
                    });
                },
                error: function(xhr) {
                    if(xhr.status === 422) {
                        let errors = xhr.responseJSON.errors;
                        $('.message').remove();
                        $.each(errors, function (key, value) {
                            const errorSelector = $("#"+key);
                            errorSelector.after("<small id='message_"+key+"' class='message text-danger form-text text-muted'>"+value+"</small>");
                        });
                    }
                }
    })
}
function getFarm(farm_id) {
    
    $.ajax({
        url: '/farm/'+ farm_id,
        type: 'get',
        dataType: 'json',
        success: function(data){
            $('#updateFarmModal').modal('show');
            $('#update_farm_id').val(data.id);
            $('#update_farm_name').val(data.farm_name);
            $('#update_farm_location').val(data.farm_location);
            $('#update_farm_distance').val(data.farm_distance);
            $('#update_farm_bags').val(data.farm_bags);
            $('#update_farm_rate').val(data.farm_rate);
            $('#update_farm_labor').val(data.farm_labor);
            $('#update_farm_type').val(data.farm_type).trigger('change');
        }
    })
}
function removeFarm(farm_id) {
    swal({
        title: "Are you sure?",
        text: "Once deleted, you will not be able to recover this data!",
        icon: "warning",
        buttons: true,
        dangerMode: true,
        }).then((result) => {
        if (result) {
          $.ajax({
                url: '/farm/'+ farm_id,
                type: 'delete',
                data: {
                    "_token": $('input[name=_token]').val()
                },
                success: function(data){
                    $('#farmDatatable').DataTable().ajax.reload();
                }
            })
        } 
    });

}
</script>

@endsection