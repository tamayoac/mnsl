<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Delivery extends Model
{
    protected $guarded = [];

    public function billing()
    {
        return $this->hasMany('App\Billing','delivery_id', 'id');
    }
    public function deliveryDetails()
    {
        return $this->hasMany('App\DeliveryDetails', 'delivery_id', 'id');
    }
    public function deliveryType()
    {
        return $this->belongsTo('App\DeliveryType', 'deliveryType_id', 'id');
    }
}
