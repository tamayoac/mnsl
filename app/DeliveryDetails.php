<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DeliveryDetails extends Model
{
    public function delivery()
    {
        return $this->belongsTo('App\Delivery', 'delivery_id', 'id');
    }
    public function farm()
    {
        return $this->belongsTo('App\Farm', 'farm_id', 'id');
    }
}
