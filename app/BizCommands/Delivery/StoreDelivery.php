<?php

namespace App\BizCommands\Delivery;

use App\DeliveryType;
use App\Delivery;
use App\Farm;

class StoreDelivery extends \App\BizCommands\BaseCommand
{
	public function doCommand($data, $usr)
	{
        $deliveryDate = $data['delivery_date'];
        $delveryType = $data['delivery_type'];
        $materialSlip = $data['delivery_materialSlip'];
        $deliveryFarm = $data['delivery_farm'];
        $deliveryQuantity = $data['delivery_quantity'];

        
        $type = DeliveryType::find($delveryType);
       
        if($type === null) {
            return $this->createReturn(404, 'Type not found');
        }

        $delivery = new Delivery();

        $delivery->code = rand();
        $delivery->delivery_date = $deliveryDate;
        $delivery->deliveryType()->associate($type);
        $delivery->save();

        $count = count($materialSlip);
       
        $deliveries = [];

        for($i = 0; $i < $count; $i++) 
        {
            $farm = Farm::find($deliveryFarm[$i]);

            $deliveries[] = [
                'delivery_id' => $delivery->id,
                'farm_id' => $farm->id,
                'material_slip' => $materialSlip[$i],  
                'actual_qty' => $deliveryQuantity[$i],
                'added_qty' => (int)$farm->farm_bags - (int)$deliveryQuantity[$i],
                'remarks' => 'Testing Remarks Here'
            ];
        }
        
        $delivery->deliveryDetails()->insert($deliveries);

        return $this->createReturn(200, 'Delivery Successfully Created', $delivery);
    }
}