<?php

namespace App\BizCommands\Farm;

use App\Farm;

class StoreFarm extends \App\BizCommands\BaseCommand
{
	public function doCommand($data, $usr)
	{
        $farm = Farm::create($data);

        return $this->createReturn(200, 'Farm Successfully Created', $farm);
    }
}