<?php

namespace App\BizCommands\Farm;

use App\Farm;

class UpdateFarm extends \App\BizCommands\BaseCommand
{
	public function doCommand($data, $id)
	{
        $farm = Farm::find($id);

        $farm->farm_name = $data['update_farm_name'];
        $farm->farm_location = $data['update_farm_location'];
        $farm->farm_distance = $data['update_farm_distance'];
        $farm->farm_bags = $data['update_farm_bags'];
        $farm->farm_labor = $data['update_farm_labor'];
        $farm->farm_rate = $data['update_farm_rate'];
        $farm->farm_type = $data['update_farm_trip'];
        $farm->save();


        return $this->createReturn(200, 'Farm Successfully Created', $farm);
    }
}