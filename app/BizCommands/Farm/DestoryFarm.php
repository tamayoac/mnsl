<?php

namespace App\BizCommands\Farm;

use App\Farm;

class DestoryFarm extends \App\BizCommands\BaseCommand
{
	public function doCommand($data, $id)
	{

        $farm = Farm::find($id)->delete();

        return $this->createReturn(200, 'Farm Successfully Created', $farm);
    }
}