<?php

namespace App\BizCommands;

class BaseCommand
{
    public function doCommand($arr, $user)
    {
        return $this->createReturn(1, "unhandled command");
    }
    public function getOrDefaultObj($arr, $key, $default)
    {
        if (isset($arr->{$key})) {
            return $arr->{$key};
        }
        return $default;
    }
    public function getOrDefaultArr($arr, $key, $default)
    {
        if (isset($arr[$key])) {
            return $arr[$key];
        }
        return $default;
    }
    public function createReturn($error_code, $message = "", $result = null)
    {
        $ret = new \stdClass();
        $ret->error_code = $error_code;
        $ret->message = $message;
        $ret->result = $result;

        return $ret;
    }
    public function execute($arr, $user)
    {
        try {
            \DB::beginTransaction();
            $ret = $this->doCommand($arr, $user);
            \DB::commit();

            return $ret;
        } catch (\Exception $exc) {
            \DB::rollback();
            $str = $exc->getMessage() . " On Line " . $exc->getLine() . " On File " . $exc->getFile();
            return $this->createReturn(2, $str);
        }
    }
    public function setJobParameters($arr, $usr)
    {
        $this->arr = $arr;
        $this->usr = $usr;
    }
    public function executeUncoupled()
    {
        return $this->executeJob($this->arr, $this->usr);
    }
}
