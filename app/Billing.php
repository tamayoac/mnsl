<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Billing extends Model
{
    public function delivery()
    {
        return $this->belongsTo('App\Delivery', 'delivery_id', 'id');
    }
}
