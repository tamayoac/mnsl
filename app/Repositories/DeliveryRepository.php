<?php

namespace App\Repositories;
use App\Delivery;
use App\DeliveryType;
use DB;

class DeliveryRepository
{
    public function getActiveFarm()
    {
        return Farm::active()->get();
    }
    public function getDeliveryType()
    {
        return DeliveryType::all();
    }
    public function getDelivery()
    {
        $deliveries = DB::table('deliveries')
                ->join('delivery_types','deliveries.deliveryType_id', '=', 'delivery_types.id')
                ->join('delivery_details', 'delivery_details.delivery_id', '=', 'deliveries.id')
                ->join('farms', 'delivery_details.farm_id', '=', 'farms.id')
                ->select('deliveries.id','deliveries.delivery_date', 'farms.farm_name','delivery_types.name as delivery_type','delivery_details.material_slip','delivery_details.actual_qty','delivery_details.remarks')
                ->get();
   

        return $deliveries;
    }
    public function getDeliveryDetails($id)
    {
        $delivery_details = Delivery::find($id)->with('deliveryDetails.farm')->first();

        return $delivery_details;
    }
}