<?php

namespace App\Repositories;
use App\Farm;

class FarmRepository
{
    public function getActiveFarm()
    {
        return Farm::active()->get();
    }
}