<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Farm extends Model
{
    protected $table = 'farms';

    protected $appends = ['status'];

    protected $fillable = ['farm_name', 'farm_location', 'farm_distance','farm_rate','farm_bags','farm_type','farm_labor'];

    protected $casts = [
        'active' => 'boolean',
      ];
    public function scopeActive($query) 
    {
        return $query->where('active', 1);
    }
    public function scopeInactive($query) 
    {
        return $query->where('active', 0);
    }
    public function getStatusAttribute()
    {
        return $this->active === true ? 'Active' : 'Inactive';

    }
    public function deliveryDetails()
    {
        return $this->hasOne('App\DeliveryDetails', 'farm_id', 'id');
    }
}
