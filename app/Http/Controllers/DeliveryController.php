<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\FarmRepository;
use App\Repositories\DeliveryRepository;
use App\Http\Requests\DeliveryStoreRequest;
use Yajra\DataTables\DataTables;
use App\Farm;
use App\DeliveryType;
use App\Delivery;

class DeliveryController extends Controller
{
    public function __construct(
        FarmRepository $farmRepository,
        DeliveryRepository $deliveryRepository
        )
    {
        $this->farmRepository = $farmRepository;
        $this->deliveryRepository = $deliveryRepository;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $farms = $this->farmRepository->getActiveFarm();
        $deliveryTypes = $this->deliveryRepository->getDeliveryType();

        if($request->ajax())
        {
            $deliveries = $this->deliveryRepository->getDelivery();
           
            return Datatables::of($deliveries)->make(true);
        }
        return view('components.delivery.index', compact('farms','deliveryTypes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(DeliveryStoreRequest $request)
    {
        $validated = $request->validated();
       
        $command1 = new \App\BizCommands\Delivery\StoreDelivery();

        $delivery = $command1->execute($validated, '');

        $command2 = new \App\BizCommands\Billing\StoreBilling();

        $billing = $command2->execute($delivery, '');

        return response()->json($delivery, $delivery->error_code);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function show($id)
    {
        $delivery_details = $this->deliveryRepository->getDeliveryDetails($id);
        
        return response()->json([$delivery_details], 200);
    }   

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
