<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\FarmStoreRequest;
use App\Http\Requests\FarmUpdateRequest;
use App\Farm;
use Yajra\DataTables\DataTables;

class FarmController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
      
        if($request->ajax()) 
        {
            $farms = Farm::all();
            return Datatables::of($farms)->make(true);
        }
        return view('components.farm.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(FarmStoreRequest $request)
    {
        $validated = $request->validated();

        $command = new \App\BizCommands\Farm\StoreFarm();

        $farm = $command->execute($validated, '');
        
        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $farm = Farm::find($id);

        return response()->json($farm);
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(FarmUpdateRequest $request, $id)
    {
        $validated = $request->validated();

        $command = new \App\BizCommands\Farm\UpdateFarm();

        $farm = $command->execute($validated, $id);

        return response()->json($farm, $farm->error_code);
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $command = new \App\BizCommands\Farm\DestoryFarm();

        $farm = $command->execute('', $id);
     
        return response()->json($farm, $farm->error_code);
    }
}
