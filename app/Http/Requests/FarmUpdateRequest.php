<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class FarmUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'update_farm_name' => 'required',
            'update_farm_location' => 'required',
            'update_farm_distance' => 'required',
            'update_farm_bags' => 'required',
            'update_farm_labor' => 'required',
            'update_farm_rate' => 'required',
            'update_farm_trip' => 'required'
        ];
    }
}
