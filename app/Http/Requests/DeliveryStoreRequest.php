<?php

namespace App\Http\Requests;
use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\Http\FormRequest;

class DeliveryStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        if(!Auth::user()) {
            return false;
        }
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'delivery_date' => 'required',
            'delivery_type' => 'required',
        ];
        if($this->request->has('delivery_materialSlip'))
        {
            foreach($this->request->get('delivery_materialSlip') as $key => $val)
            {
                $rules['delivery_materialSlip.'.$key] = 'required|unique:delivery_details,material_slip';
            }
        }
        if($this->request->has('delivery_farm'))
        {
            foreach($this->request->get('delivery_farm') as $key => $val)
            {
                $rules['delivery_farm.'.$key] = 'required';
            }
        }
        if($this->request->has('delivery_quantity'))
        {
            foreach($this->request->get('delivery_quantity') as $key => $val)
            {
                $rules['delivery_quantity.'.$key] = 'required';
            }
        }
        return $rules;
    }
}
