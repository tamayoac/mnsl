<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DeliveryType extends Model
{
    public function delivery()
    {
        return $this->hasOne('App\Delivery', 'deliveryType_id', 'id');
    }
}
