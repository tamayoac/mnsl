<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;
use App\User;
class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::insert([
        [
            'id' => 1,
            'name' => 'Alfred',
            'email' => 'kedafen421@ofmailer.net',
            'password' => bcrypt("password"),
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]
        ]);
    }
}
