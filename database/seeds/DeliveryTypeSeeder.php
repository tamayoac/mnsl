<?php

use Illuminate\Database\Seeder;
use App\DeliveryType;

class DeliveryTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DeliveryType::insert([
            [
                'id' => 1,
                'name' => 'Connecting'
            ],
            [
                'id' => 2,
                'name' => 'Divert'
            ],
            [
                'id' => 3,
                'name' => 'Retrival'
            ]
        ]);
    }
}
