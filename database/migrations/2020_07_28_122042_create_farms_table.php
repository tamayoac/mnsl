<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFarmsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('farms', function (Blueprint $table) {
            $table->increments('id');
            $table->string('farm_name');
            $table->string('farm_location');
            $table->decimal('farm_distance');
            $table->decimal('farm_rate');
            $table->integer('farm_bags');
            $table->enum('farm_type',['REGULAR','SPECIAL'])->default('REGULAR');
            $table->decimal('farm_labor');
            $table->boolean('active')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('farms');
    }
}
