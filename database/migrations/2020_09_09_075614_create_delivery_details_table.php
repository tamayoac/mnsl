<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDeliveryDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('delivery_details', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('delivery_id')->unsigned()->index();
            $table->foreign('delivery_id')->references('id')->on('deliveries')->onCascade('delete'); 
            $table->integer('farm_id')->unsigned()->index();
            $table->foreign('farm_id')->references('id')->on('farms')->onCascade('delete'); 
            $table->string('material_slip')->unique();
            $table->integer('actual_qty');
            $table->integer('added_qty');
            $table->string('remarks');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('delivery_details');
    }
}
