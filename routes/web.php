<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');


// F A R M
Route::group(['middleware' => 'auth'], function() {
    Route::group(['prefix' => 'farm'], function() {
        Route::get('/', 'FarmController@index')->name('farm.index');
        Route::get('/{id}', 'FarmController@edit');
        Route::post('/store', 'FarmController@store')->name('farmstore');
        Route::put('/{id}', 'FarmController@update')->name('farmupdate');
        Route::delete('/{id}', 'FarmController@destroy');
    });
    Route::group(['prefix' => 'delivery'], function() {
        Route::get('/', 'DeliveryController@index')->name('delivery.index');
        Route::get('/{id}', 'DeliveryController@show');
        Route::post('/store', 'DeliveryController@store')->name('delivery.store');
       
    });
});

